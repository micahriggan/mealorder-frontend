var express = require('express');
var app = express();
var session = require('express-session');
var mongoose = require('mongoose'); 
mongoose.connect('mongodb://db');
var MongoStore = require("connect-mongo")(session);
var cookieParser = require('cookie-parser');


app.use(cookieParser('biscuits are delicious'));
app.use(session({
	secret: 'biscuits are delicious',
	resave: true,
	saveUninitialized: true,
	store: new MongoStore({
			mongooseConnection: mongoose.connection
		},
		function(err) {
			console.log(err || "Mongo store setup fine");
		}),
	cookie: {
		maxAge: 60 * 1000 * 60
	}
}));


app.use(express.static(__dirname + '/dist'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.listen(3002);
