'use strict';

var path = require('path');
var gulp = require('gulp');
var concat = require('gulp-concat');
var conf = require('./conf');
var ngConstant = require('gulp-ng-constant');
gulp.task('constants', function () {
    // Writes gulpConstants.js to app/ folder
    var backendUrl = process.env.BACKEND_URL || "http://localhost:9000";
	return ngConstant({
      name: 'gulpcon',
      constants: {
		  backend: backendUrl,
		  clientTokenPath: '/payment/token'
	  },
      stream : true
    })
    .pipe(concat('gulpConstants.js'))
    .pipe(gulp.dest(path.join(conf.paths.src, '/app/')));
  
});
