(function() {
	'use strict';
	angular.module('code').controller('UserController', UserController); /** @ngInject */

	function UserController($scope, $state, userService, api) {

		$scope.isEditingProfile = false;
		$scope.editProfile = function() {
			$scope.isEditingProfile = true;
		};
		$scope.cancelEditProfile = function() {
			$scope.isEditingProfile = false;
		};
		$scope.saveProfileChanges = function() {

			$scope.user.subscription = $scope.editedSubscription;

			// not quite sure if I'm doing this part in the right place

			userService.updateUserInfo($scope.user).success(function() {
				userService.getCurrentUser().then(function(user) {
					$scope.user = user;
					$scope.cancelEditProfile();
				});
			});
		};
		$scope.logoutUser = function(callback) {

			userService.logoutUser().success(function() {
				if (callback) callback();
			});
		};

		$scope.getReferLink = function(refer){
			console.log(refer);
			return $state.href('register-refer', { referredBy  : refer}, {absolute : true });
		};
		if (!$scope.user) {
			userService.getCurrentUser().then(function(user) {
				$scope.user = user;
				api.get('/payment/plans').success(function(plans) {
					console.log(plans);
					$scope.subscriptions = plans.plans;
					$scope.user.subscription = _.findWhere(plans.plans, {
						id: $scope.user.planId
					});
				});


			});
			api.get('/payment/customer').success(function(resp) {
				console.log(resp);
				$scope.creditCards = resp.customer.creditCards;
			});


			api.get('/order').success(function(resp) {
				console.log(resp);
				$scope.orders = resp.orders;
			});
		}

	}
})();
