(function()
{
	'use strict';

	angular.module('code').controller('FAQController', FAQController); /** @ngInject */

	function FAQController($scope, $location, $anchorScroll)
	{
		$scope.faqs = {
			
			"Where are you currently delivering?":
			"Our delivery area currently includes select neighborhoods in Spring Hill, TN. Check availability of delivery in your area by entering your street and zip code on our homepage. We are growing quickly and will be delivering to other towns & neighborhoods soon.",
			
			"How often are meals delivered?":
			"Currently HomeBiscuit is delivering  meals to your door on Tuesday and Thursday evenings.  As we continue to grow we will increase delivery to 4 nights per week.",
			
			"How much does it cost?":
			"We offer several options allowing you to choose which is best for you and your family. Our Premium Members pay $10 per meal, tax and tip included. Our Valued Members pay $12 per meal, tax and tip included. If either of our Premium or Valued Members pre-pay for three months they receive an additional 10% off or $9 per meal, tax and tip included and $10.80 per meal, tax and tip included respectively. If you would prefer to order as needed our Trial customers pay $15 per meal tax and tip included. We do charge a small $3 delivery fee on all orders.",
			
			"Where and how are meals prepared?":
			"HomeBiscuit works with local professional caterers who prepare the meals in commercial kitchens.  They use strict cooking procedures to ensure the best and safest possible meals for you and your family.",
			
			"How big are the portions?":
			"Our meals are typically 14 to 16 ounces per individually portioned serving.  We always strive to provide a great value to our customers.",
			
			"Do you have options for people with food allergies or dietary needs?":
			"Currently we do not. Please see our Food Allergy Disclaimer for details.",
			
			"What are the terms of the membership programs?":
			"There are no long term contracts and you can cancel at anytime. Our discounted membership plans have a minimum 4 weeks of service, and your account is automatically renewed for your convenience.   To end our service simply contact us 5 days in advance of your renewal date.  See our service agreement for details. We know how busy life can be, so for your convenience we also include  Auto Chef. In the event you forget to order, Auto Chef will automatically select a meal and delivery day from one of your previous orders, which we gladly deliver to your door. If you have no previous orders than one of our chefs will choose a delicious meal for you.",
			
			"What if I forget to order?":
			"When you sign up for one of our membership services you will also be asked to enroll in Auto Chef. This will ensure that in the event you forget, or are simply unable to order, we will automatically deliver a meal to your home, so your family does not miss a meal. The meal will be selected from one of your previous orders you've chosen and delivered on the same day of the week as a previous order. In the event you do not have a previous menu item available, one of our chefs will select a delicious meal for you.",
			
			"How does the Referral program work?":
			"When you sign up for one of our membership services you are assigned a referral code. Each time you refer a friend or neighbor and they sign up for one of our membership services using your referral code you will receive a FREE meal. If you refer a customer or customers and they place a trial order, you will receive a FREE meal once they have purchased at least 4 meals using your referral code. The credit will be applied to your account automatically on the next billing cycle. There is no limit to the amount of FREE meals you can receive, so please share our service with as many neighbors as possible. For more details please see the Referral Program section of our service agreement.",
			
			"I received a Referral Code/Promotional Code, where do I enter this information?":
			"When you sign up as a customer, you will have the opportunity to enter your referral code or promotional code, this will qualify you or the person that referred you to receive a specific promotional offer.  The corresponding credit will be applied to your account at time of check out or on the next billing cycle if you are a member.",
			
			"What if I am not home during the delivery time?":
			"If you are not home at the time of delivery, no worries, we will leave your meal on your doorstep in an insulated bag or follow your instructions.  If you think you will not be able to collect your food for more than 1 hour after delivery than we strongly encourage you to leave a cooler with ice on your doorstep. The meals are packaged in microwave safe containers and can easily be reheated.   They can also be refrigerated to enjoy within four to five days of delivery. If you are going on vacation or need to pause the service for any reason, simply let us know the dates and we will gladly hold your service until you return.",
			
			"What if I live in a secure building or gated community?":
			"To ensure we are able to deliver to customers in secure buildings or gated communities, and stay on our schedule, we require an access code, key, or other means of entry so we can leave your dinner at your door if you do not answer.",
			
			"How long will the meals keep in my fridge or freezer?":
			"Our meals should be just as good when properly refrigerated for up to 4 days, or frozen for up to two weeks. They can then be reheated in the microwave, or transferred to another container for oven heating.",
			
			"How do I sign up?":
			"Sign up online here now to get started. Or call us at 615-829-6210 and we will be happy to assist you."

		};
		
		$scope.jumpToQuestion = function(hash) {
			
			$location.hash(hash);
			
			$anchorScroll();
		};
	}
})();
