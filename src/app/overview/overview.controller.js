(function() {
	'use strict';
	angular.module('code').controller('OverviewController', OverviewController); /** @ngInject */

	function OverviewController($scope, $state, api) {
		
		api.get('/payment/plans').success(function(data) {
			$scope.subscriptions = data.plans;
		});
	}
})();