(function() {
  'use strict';

  angular
    .module('code')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope) {

    $log.debug('runBlock end');
    
    $rootScope.facebookAppId = '[FacebookAppId]';
    
  }

})();
