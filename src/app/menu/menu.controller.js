(function() {
	'use strict';

	angular.module('code').controller('MenuController', MenuController); /** @ngInject */

	function MenuController($scope, menuItemsService, $mdDialog, $mdSidenav, $timeout, userService, api) {

		if (!$scope.user) {
			userService.getCurrentUser().then(function(user) {
				$scope.user = user;

			});
		}

		// Public(?)
		$scope.meals = {};
		$scope.addMenuItem = function(menuItem) {
			var mealWithOpenSlot = null;
			if (!(menuItem.date in $scope.meals)) {
				$scope.meals[menuItem.date] = [];
			}
			// console.log("Adding meal to ", menuItem.date);
			var dayMeals = $scope.meals[menuItem.date];
			// loop through meals in order to determine available menu item slot
			for (var i = 0; i < $scope.meals[menuItem.date].length; ++i) {
				var meal = $scope.meals[menuItem.date][i];
				// console.log("Cycling meals: ", meal);
				if (!meal.entree && menuItem.item.type === "entree") {
					// console.log("Added entree to an existing meal: ", meal);
					meal.entree = menuItem;
					mealWithOpenSlot = meal;
					break;
				} else if (meal.sides.length < 2 && menuItem.item.type === "side") {
					// console.log("Added side to an existing meal: ", meal);
					console.log(menuItem);
					meal.sides.push(menuItem);
					mealWithOpenSlot = meal;
					break;
				}
			}
			if (!mealWithOpenSlot) { // never found an open meal, make a new one
				mealWithOpenSlot = new Meal(null, []);
				mealWithOpenSlot.addMenuItem(menuItem);
				$scope.meals[menuItem.date].push(mealWithOpenSlot);
				console.log("Created new meal: ", mealWithOpenSlot);
			}


			// open side nav if closed
			$scope.openOrderSideNav();

			// close side nav on timeout
			// $timeout($scope.closeOrderSideNav, 2500);
		};
		$scope.removeMenuItem = function(menuItem, fromMeal) {
			if (menuItem === fromMeal.entree) {
				fromMeal.entree = null;
			} else {
				for (var i = 0; i < fromMeal.sides.length; ++i) {
					if (menuItem === fromMeal.sides[i]) {
						fromMeal.sides.splice(i, 1);
					}
				}

				if (!fromMeal.sides.length) {
					fromMeal.sides = [];
				}
			}

			cleanupOrder();
		};
		$scope.openOrderSideNav = function() {
			$mdSidenav("orderSideNav").open();
		};
		$scope.closeOrderSideNav = function() {
			$mdSidenav("orderSideNav").close();
		};

		$scope.mealCount = function() {

			var size = _.reduce(_.keys($scope.meals),
				function(memo, num) {
					return memo + $scope.meals[num].length;
				}, 0);
			return size;
		};

		// Private(?)

		function Meal(entree, sides) {
			this.entree = entree;
			this.sides = sides;
			this.addMenuItem = function(orderItem) {
				// console.log("Adding new menu item to meal.");
				if (orderItem.item.type === "entree") {
					this.entree = orderItem;
				} else if (orderItem.item.type === "side") {
					this.sides.push(orderItem);
				}
			};

			return this;
		}

		function cleanupOrder() {
			for (var day in $scope.meals) {
				console.log(day);
				for (var i = 0; i < $scope.meals[day].length; ++i) {
					var meal = $scope.meals[day][i];

					if (meal.entree === null && !meal.sides.length) {

						$scope.meals[day].splice($scope.meals[day].indexOf(meal), 1);
						console.log("Deleted meal from day: ", $scope.meals[day]);
					}
				}

				if (!$scope.meals[day].length) {
					delete $scope.meals[day];
				}
			}
			console.log("Order cleanup done: ", $scope.meals);
		}
		// Execution
		menuItemsService.getMenu().success(function(data) {
			console.log("Menu", data);
			$scope.menuItems = data;
		});



		$scope.showCheckout = function(ev) {
			$mdDialog.show({
					controller: 'CheckoutController',
					templateUrl: 'app/checkout/checkout.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					locals: {
						meals: $scope.meals
					},
					clickOutsideToClose: true
				})
				.then(function(submitted) {
					if (submitted){
						$scope.meals = {};
						
						// here's where to show the success guy
						toastr.success("Order Received", "Order submitted successfully!");
						
						console.log("Submitted order");
					}
				}, function() {
					console.log("Canceled dialog");
				});
		};
	}

})();
