(function() {
	
	'use strict';
	
	angular
	.module('code')
	.controller('HomeController', HomeController);
	
	
	/** @ngInject */
	function HomeController($scope) {
		
		$scope.verifyAddress = function(addressString) {
			console.log(addressString);
		}
		
	}
	
})();