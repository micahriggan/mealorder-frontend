(function() {
	'use strict';
	angular.module('code').config(routeConfig); /** @ngInject */

	function routeConfig($stateProvider, $urlRouterProvider) {
		$stateProvider.state('home', {
				url: '/',
				templateUrl: 'app/home/home.html',
				controller: 'HomeController',
				controllerAs: 'home'
			}).state('overview', {
				url: '/overview',
				templateUrl: 'app/overview/overview.html',
				controller: 'OverviewController',
				controllerAs: 'overview'
			}).state('menu', {
				url: '/menu',
				templateUrl: 'app/menu/menu.html',
				controller: 'MenuController',
				controllerAs: 'menu'
			}).state('contact', {
				url: '/contact',
				templateUrl: 'app/contact/contact.html',
				controller: 'ContactController',
				controllerAs: 'contact'
			}).state('faq', {
				url: '/faq',
				templateUrl: 'app/faq/faq.html',
				controller: 'FAQController',
				controllerAs: 'faq'
			}).state('login', {
				url: '/login',
				templateUrl: 'app/login/login.html',
				controller: 'LoginController',
				controllerAs: 'login'
			})
			.state('user', {
				url: '/user',
				templateUrl: 'app/user/user.html',
				controller: 'UserController',
				controllerAs: 'user'
			})
			.state('checkout', {
				url: '/checkout',
				templateUrl: 'app/checkout/checkout.html',
				controller: 'CheckoutController',
				controllerAs: 'checkout'
			})
			.state('authorized', {
				url: '/authorized/:token',
				templateUrl: 'app/authorized/authorized.html',
				controller: 'AuthorizedController',
				controllerAs: 'authorized'
			})
			.state('admin', {
				url: '/admin',
				templateUrl: 'app/admin/admin.html',
				controller: 'AdminController',
				controllerAs: 'admin'
			})
			.state('register', {
				url: '/register',
				templateUrl: 'app/register/register.html',
				controller: 'RegisterController',
				controllerAs: 'register'
			})
			.state('register-refer', {
				url: '/register/:referredBy',
				templateUrl: 'app/register/register.html',
				controller: 'RegisterController',
				controllerAs: 'register'
			})
			.state('terms-and-conditions', {
				url: '/terms-and-conditions',
				templateUrl: 'app/terms-and-conditions/terms-and-conditions.html',
				controller: 'TermsAndConditionsController',
				controllerAs: 'terms-and-conditions'
			})
			.state('privacy-policy', {
				url: '/privacy-policy',
				templateUrl: 'app/privacy-policy/privacy-policy.html',
				controller: 'PrivacyPolicyController',
				controllerAs: 'privacy-policy'
			});

		$urlRouterProvider.otherwise('/');
	}
})();
