(function() {
  'use strict';

  angular
    .module('code')
    .directive('acmeMalarkey', acmeMalarkey);

  /** @ngInject */
  function acmeMalarkey(malarkey) {
    var directive = {
      restrict: 'E',
      scope: {
        items: '=',
        bind: '@'
      },
      template: '&nbsp;',
      link: linkFunc,
      controller: MalarkeyController,
      controllerAs: 'vm'
    };

    return directive;

    function linkFunc(scope, el, attr, vm) {
      var watcher;
      var typist = malarkey(el[0], {
        typeSpeed: 40,
        deleteSpeed: 40,
        pauseDelay: 1200,
        loop: true,
        postfix: ' '
      });

      el.addClass('acme-malarkey');

      console.log(scope.extraValues);
      angular.forEach(scope.items, function(value) {
        console.log(value);
        typist.type(value).pause().delete();
      });

      watcher = scope.$watch('scope.bind', function() {
        angular.forEach(scope.bind, function(item) {
          typist.type(item).pause().delete();
        });
      });

      scope.$on('$destroy', function() {
        watcher();
      });
    }

    /** @ngInject */
    function MalarkeyController($log) {
      var vm = this;

      vm.items = ['Blue Bob'];

      getData();

      function getData() {
        return vm.items;
      }

    }

  }

})();
