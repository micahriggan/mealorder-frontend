(function() {
	'use strict';
	angular.module('code')
		.service('menuItemsService', menuItemsService);

	/** @ngInject */


	function menuItemsService(api) {
		var data;
		return {
			getMenuItems : function() {

				return api.get("/menu/items");

			},
			setMenuItems : function(data) {
				
				return api.post("/menu/items", {
					menuItems: data
				});
				
			},
			getMenu : function() {
				return api.get("/menu");
			},
			setMenu : function(data) {
				
				return api.post("/menu", {
					menu: data
				});
			},
			
			getOrders : function(startDate, endDate) {
				
				return api.get("/order/between/" + startDate + "/" + endDate);
			}
		};

	}
})();
