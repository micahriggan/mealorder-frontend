(function() {
	'use strict';
	angular.module('code').service('userService', function($cookies, $q, api) {
		var user;
		this.getUserById = function(userID) {
			return $q(function(resolve, reject) {
				if (!user) {
					console.log("Getting user");
					api.get('/user/' + userID).then(function(resp) {
						user = resp.data;
						resolve(resp.data);
					}, function(err) {
						reject(err);
					});
				} else {
					resolve(user);
				}
			});
		};
		this.getCurrentUser = function() {
			return $q(function(resolve, reject) {
				if (!user) {
					console.log("Getting user");
					api.get('/user').then(function(resp) {
						// console.log("resolving ", resp.data);
						user = resp.data;
						resolve(resp.data);
					}, function(err) {
						// console.log("rejecting ", err);
						reject(err);
					});
				} else {
					resolve(user);
				}
			});
		};

		this.setUser = function(newUser) {
			user = newUser;
		};

		this.updateUserInfo = function(updates) {
			user = null;
			//ensures that the user is reloaded from the db after update
			return api.post("/user/update", {
				user: updates
			});

		};

		this.logoutUser = function() {
			user = null;
			api.setToken(null);
			return api.get('/user/logout');
		};



	}); /** @ngInject */



})();
