// this file will be used to create authorized requests to the homebiscuit backend
//
// It will contain methods that allow you to get and post to routes in the backend 
// 	it will auto concat the backend url to all the requests
// 	it will auto fill the token to the headers if it has been obtained
//
// TODO check for a token, if one isn't present, show a toast telling the user to login for that feature
(function() {
		'use strict';
		angular.module('code')
			.service('api', function($http, $cookies, backend) {
				var token = null; 

				this.setToken = function(newToken) {
					token = newToken;
					$cookies.put("token", newToken);
				};

				this.getToken = function() {
					// console.log("token : " + token + " cookie : " + $cookies.get("token"));
					token = token || $cookies.get("token") ;
					return token;

				};
				this.get = function(route, config) {
					config = config || {};
					if (this.getToken()) {
						// console.log("Token : ", token);
						config.headers = {
							'Authorization': 'Bearer ' + token
						};
						config.withCredentials = true;
					} else config = {};
					// console.log(backend + route, config);
					return $http.get(backend + route, config);
				};
				this.post = function(route, data, config) {
					config = config || {};
					data = data || {};
					if (this.getToken()) {
						config.headers = {
							'Authorization': 'Bearer ' + token
						};
						config.withCredentials = true;
					}
					// console.log(backend + route, config);
					return $http.post(backend + route, data, config);
				};

			});
	}


	/** @ngInject */



)();
