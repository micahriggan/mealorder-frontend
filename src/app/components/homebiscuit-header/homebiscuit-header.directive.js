(function() {
	'use strict';
	angular.module('code').directive('homebiscuitHeader', homebiscuitHeader);

	/** @ngInject */

	function homebiscuitHeader() {
		var directive = {
			restrict: 'E',
			templateUrl: 'app/components/homebiscuit-header/homebiscuit-header.html',
			scope: {},
			controller: HomeBiscuitHeaderController,
			controllerAs: 'header',
			bindToController: true
		};

		/** @ngInject */

		function HomeBiscuitHeaderController($scope, $mdDialog, $state, userService) {
			var header = this;
			var originatorEv;
			header.openMenu = function($mdOpenMenu, ev) {
				console.log("menu open click");
				originatorEv = ev;
				$mdOpenMenu(ev);
			};

			header.go = function(state) {
				// console.log("going to " + state);
				$state.go(state);
			};

			if (!$scope.user) {
				userService.getCurrentUser().then(function(user) {
					$scope.user = user;
				});
			}
			
			$scope.logoutUser = function(callback) {
				
				userService.logoutUser().success(function() {
					if (callback) callback();
				});
			}
		}

		return directive;

	}
})();
