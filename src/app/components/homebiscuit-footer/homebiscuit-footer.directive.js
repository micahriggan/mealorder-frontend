(function()
{
	'use strict';
	angular.module('code').directive('homebiscuitFooter', homebiscuitFooter);
	
	/** @ngInject */

	function homebiscuitFooter()
	{
		var directive = {
			restrict: 'E',
			templateUrl: 'app/components/homebiscuit-footer/homebiscuit-footer.html',
			scope: {},
			controller: HomeBiscuitFooterController,
			controllerAs: 'homebiscuit-footer',
			bindToController: true
		};
		
		/** @ngInject */

		function HomeBiscuitFooterController($scope)
		{
			var footer = this;
			
			$scope.model = {
              Url: 'http://homebiscuit.com/',
              Name: "HomeBiscuit",
              ImageUrl: 'assets/images/homebiscuit-logo.png'
          };
		}
		
		return directive;

	}
})();