(function() {
	'use strict';

	angular
		.module('code')
		.controller('CheckoutController', CheckoutController);

	/** @nginject */
	function CheckoutController($scope, api, $braintree, clientTokenPath, meals, userService, $mdDialog) {
		$scope.order = {};
		if (!$scope.user) {
			userService.getCurrentUser().then(function(user) {
				$scope.user = user;

				$scope.order.address = $scope.user.street + ' ' + $scope.user.city + ' ' + $scope.user.state + ' ' + $scope.user.zip;
			});
		} else {

			$scope.order.address = $scope.user.street + ' ' + $scope.user.city + ' ' + $scope.user.state + ' ' + $scope.user.zip;
		}


		var prepareOrder = function(mealsDict) {

			function transform(meal) {
				var side1 = meal.sides.length > 0 ? meal.sides[0]._id : null;
				var side2 = meal.sides.length > 1 ? meal.sides[1]._id : null;
				return {
					entree: meal.entree._id,
					side1: side1,
					side2: side2
				};
			}
			return _.chain(mealsDict).values(mealsDict).flatten().map(transform).value();
		};
		
		
		
		
		
		$scope.checkOut = function() {

			$scope.order.items = prepareOrder(meals);
			console.log("Orders! ", $scope.order);
			api.post("/order", {
				order: $scope.order
			}).then(function(resp) {
				console.log(resp);
				$mdDialog.hide(true);
			}, function(err) {
				console.log(err);
			});
		};
		$scope.close = function() {
			$mdDialog.cancel();
		};
		$scope.getTotal = function(numItems) {
			var total = 0;
			console.log("number of Items: ", numItems);
			for (var i = 1; i <= numItems; i++) {
				total += $scope.getPrice(i);
				console.log("checking price for item ", i);
			}
			console.log("Total", total);
			return total;
		};

		$scope.getPrice = function(itemNumber) {
			return $scope.pricingInfo.subscriptionMealsLeft - itemNumber >= 0 ? 0 : $scope.pricingInfo.addonPrice;
		};
		api.get('/payment/itemprice').success(function(data) {
			$scope.meals = meals;
			console.log("Meals ", meals);
			console.log(data);
			$scope.pricingInfo = data.pricingInfo;
			var allMeals = _.chain($scope.meals).values($scope.meals).flatten().value();
			$scope.total = $scope.getTotal(allMeals.length);
		});
		/*     api.get(clientTokenPath).success(function(data) {*/
		//console.log(data);
		//$braintree.setup(data.token, "dropin", {
		//container: "payment-form"
		//});


		/*});*/
	}
})();
