(function() {
  'use strict';

  angular
    .module('code')
    .config(config);

  /** @ngInject */
  function config($logProvider, $mdThemingProvider, toastr) {
    // Enable log
    $logProvider.debugEnabled(true);

    // theme
    $mdThemingProvider.theme('default')
    .primaryPalette('indigo')
    .accentPalette('grey');

    // Set options third-party lib
    toastr.options.timeOut = 5000;
    toastr.options.preventDuplicates = true;
    toastr.options.progressBar = true;
  }

})();
