(function() {

	'use strict';

	/** @ng-inject */


	angular.module('code')
		.controller('AuthorizedController', AuthorizedController);


	function AuthorizedController($scope, $state, $stateParams, api, userService) {
		console.log("stateParams", $stateParams);
		$scope.token = $stateParams.token;
		api.setToken($stateParams.token);
		api.get("/user").success(function(result) {
			console.log(result);
			userService.setUser(result);
			$scope.braintree = result;
			$state.go('menu');
		});
		//Get user here and call setUser()

	}


})();
