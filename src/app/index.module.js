(function() {
	'use strict';

	angular
		.module('code', ['gulpcon', 'ngAnimate', 'ngAria', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ngMaterial', 'angular.filter', 'ngMdIcons', 'angulike', 'braintree-angular', 'ngMessages']);

})();
