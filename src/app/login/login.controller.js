(function() {
	'use strict';

	angular.module('code').controller('LoginController', LoginController); /** @ngInject */

	function LoginController($scope, $window, $state, api, backend) {
		$scope.loginError = "";
		$scope.loginUser = function() {
			api.post('/auth/local', $scope.user).then(function(resp) {
				console.log("login stuffs ", resp);
				$state.go('authorized', resp.data);
			}, function(errResp){
				console.log(errResp);
				$scope.loginError = "Authentication Failed";	
			});

		};

		$scope.backend = backend;
		// $scope.open = function(url){
		// 	$window.open(url, "Please Login");
		// 	//need to emit a message when the load is completed
		// }
	}

})();
