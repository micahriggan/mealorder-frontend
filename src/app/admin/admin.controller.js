(function() {
	'use strict';
	angular.module('code').controller('AdminController', AdminController); /** @ngInject */

	function AdminController($scope, api, menuItemsService, userService) {

		// Variables
		var date = new Date();
		var beginOfMonth = new Date(date.getFullYear(), date.getMonth(), 0);
		var endOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);

		$scope.orders = [];
		$scope.ordersWithUser = [];
		$scope.menu = [];
		$scope.menuHasBeenEdited = false;
		$scope.menuConfigurationSelectedDateMenu = [];
		$scope.availableMenuItems = [];
		$scope.editedMenuItems = [];
		$scope.menuItemsHaveBeenEdited = false;
		$scope.newMenuItem = null;
		$scope.menuConfigurationSelectedDate = moment.utc().toDate();
		console.log("Selected date: ", $scope.menuConfigurationSelectedDate);
		$scope.minDate = beginOfMonth;
		$scope.userOrdersStartDate = beginOfMonth;
		console.log($scope.userOrdersStartDate);
		$scope.userOrdersEndDate = endOfMonth;
		$scope.userOrdersSorting = 'name';
		$scope.shouldShowHiddenMenuItems = false;
		$scope.menuSearchCriteria = "";
		$scope.menuItemSearchCriteria = "";

		// watch selected date for changes
		$scope.$watch('menuConfigurationSelectedDate', function() {
			// console.log($scope.menuConfigurationSelectedDate);
			updateAvailableItemsForSelectedDate();
		});
		$scope.$watch('userOrdersStartDate', function() {
			$scope.userOrdersMinEndDate = $scope.userOrdersStartDate;
			$scope.userOrdersMinEndDate.setDate($scope.userOrdersMinEndDate.getDate() + 1);

			// check if user orders end date is after start date
			if ($scope.userOrdersEndDate <= $scope.userOrdersStartDate)
				$scope.userOrdersEndDate = $scope.userOrdersMinEndDate;
			$scope.getOrders();
		});
		$scope.$watch('userOrdersEndDate', function() {
			// reload orders
			$scope.getOrders();
		});


		// Data Structures
		function MenuItem(name, description, image, type) {
			return {
				name: name || '',
				description: description || '',
				image: image || '',
				type: type || '',
				isAvailableOnSelectedDate: false
			};
		}

		function OrderItem(item) {
			return {
				date: $scope.menuConfigurationSelectedDate.toISOString().split("T")[0],
				item: item
			};
		}



		// Data Handlers
		$scope.getOrders = function() {
			api.get('/order/between/' + $scope.userOrdersStartDate + "/" + $scope.userOrdersEndDate).success(function(data) {
				console.log(data);
				$scope.orders = data.orders;
				$scope.groupedOrders = _.groupBy($scope.orders, function(order) {
					console.log("grouping ", order);
					return order.items[0].entree.date.split("T")[0];
				});


			});
		};
		$scope.getMenu = function() {
			menuItemsService.getMenu().success(function(data) {
				// the times are being stripped out of the dates here
				$scope.menu = _.map(data, function(item) {
					item.date = item.date.split("T")[0];
					return item;
				});

				$scope.menuHasBeenEdited = false;
				// console.log("Menu before edit: ", $scope.menu);
				// console.log("Edited menu before edit: ", $scope.menu);
				updateAvailableItemsForSelectedDate();
			});
		};
		$scope.submitMenu = function() {
			menuItemsService.setMenu($scope.menu).success(function() {
				$scope.getMenu();

			});
		};
		$scope.getMenuItems = function() {

			menuItemsService.getMenuItems().success(function(data) {
				$scope.availableMenuItems = data;

				$scope.editedMenuItems = [];
				$scope.menuItemsHaveBeenEdited = false;
				$scope.newMenuItem = null;

				$scope.editedMenuItems = _.map($scope.availableMenuItems, function(item) {
					return _.omit(item, 'isAvailableOnSelectedDate');
				});

				updateAvailableItemsForSelectedDate();

			});
		};
			$scope.submitEditedMenuItems = function() {

			menuItemsService.setMenuItems($scope.editedMenuItems).success(function() {
				$scope.getMenuItems();

			});
		};
		$scope.markOrderDelivered = function(order) {

			api.get("/order/deliver/" + order._id).success(function() {
				order.delivered = true;
			});
		};
		$scope.menuItemSearchFilter = function(item) {
			return $scope.menuItemSearchCriteria === "" || (item.name.toLowerCase().indexOf($scope.menuItemSearchCriteria.toLowerCase()) != -1);
		};




		// Menu Editing
		function updateAvailableItemsForSelectedDate() {
			// the order items for the selected date
			console.log("Testing date : ", $scope.menuConfigurationSelectedDate.toISOString().split("T")[0]);
			$scope.menuConfigurationSelectedDateMenu = _.where($scope.menu, {
				date: $scope.menuConfigurationSelectedDate.toISOString().split("T")[0]
			});

			console.log("Menu: ", $scope.menu);
			console.log("Available menu items: ", $scope.availableMenuItems);
			console.log("Selected date menu items: ", $scope.menuConfigurationSelectedDateMenu);

			$scope.availableMenuItems = _.map($scope.availableMenuItems, function(menuItem) {
				//console.log("Current item: ", menuItem);

				var found = _.find($scope.menuConfigurationSelectedDateMenu, function(orderItem) {
					return orderItem.item.name == menuItem.name;
				});
				//console.log("Found: ", found);

				// need to iterate over the menu and determine the existence of menu item
				menuItem.isAvailableOnSelectedDate = (found !== undefined);

				return menuItem;
			});
		}
		$scope.menuFilter = function(item) {
			return ($scope.menuSearchCriteria === "" || item.name.toLowerCase().indexOf($scope.menuSearchCriteria.toLowerCase()) != -1) && ($scope.shouldShowHiddenMenuItems || !item.hidden);
		};
		$scope.toggleExistenceInSelectedMenu = function(menuItem) {

			// console.log("Menu item in question: ", menuItem);
			$scope.menuHasBeenEdited = true;

			// check if order item is already in the menu
			if (!menuItem.isAvailableOnSelectedDate) {
				$scope.menu.push(new OrderItem(_.omit(menuItem, "isAvailableOnSelectedDate")));
			} else {
				$scope.menu = _.filter($scope.menu, function(orderItem) {
					var differentDay = orderItem.date !== $scope.menuConfigurationSelectedDate.toISOString().split("T")[0];
					var differentName = orderItem.item.name !== menuItem.name;
					return differentDay || differentName;
				});
			}

			// console.log("Menu: ", $scope.menu);

			updateAvailableItemsForSelectedDate();
		};



		// Menu Items Editing	
		$scope.addMenuItem = function() {
			$scope.newMenuItem = new MenuItem();
		};
		$scope.confirmMenuItem = function() {

			$scope.editedMenuItems.push($scope.newMenuItem);
			$scope.menuItemsHaveBeenEdited = true;
			$scope.newMenuItem = null;

		};
		$scope.didToggleMenuItem = function(menuItem) {

			//$scope.editedMenuItems = _.filter($scope.editedMenuItems, function(item) {
			//return item !== menuItem;
			//});

			//// also remove any instances of order items matching menu item in menu
			//$scope.menu = _.filter($scope.menu, function(orderItem) {
			//return orderItem.item.name !== menuItem.name;
			//});

			$scope.menuItemsHaveBeenEdited = true;
		};
		$scope.editedMenuItemsAreValid = function() {

			for (var i = 0; i < $scope.editedMenuItems.length; ++i) {
				if ($scope.editedMenuItems[i].name.length <= 0 || $scope.editedMenuItems[i].description.length <= 0 || $scope.editedMenuItems[i].type.length <= 0) {
					return false;
				}
			}

			return true;
		};



		// Execution
		$scope.getOrders();
		$scope.getMenuItems();
		$scope.getMenu();
	}
})();
