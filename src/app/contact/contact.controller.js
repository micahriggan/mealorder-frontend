(function() {
	'use strict';
	angular.module('code').controller('ContactController', ContactController); /** @ngInject */

	function ContactController($scope, $http, backend) {
		$scope.submit = function() {
			console.log($scope.contact);
			$http.post(backend + '/contact', {
				contact: $scope.contact
			});
		};
	}
})();