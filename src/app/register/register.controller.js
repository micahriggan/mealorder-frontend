(function() {
	'use strict';
	angular.module('code').controller('RegisterController', RegisterController); /** @ngInject */

	function RegisterController($scope, $state, $stateParams, $sceDelegate, userService, api, backend, $braintree, clientTokenPath) {
		$scope.paymentReceived = false;
		$scope.user = {};
		$scope.user.planId = "None";
		console.log($stateParams);
		$scope.referredBy = $stateParams.referredBy;
		$scope.submitUrl = $sceDelegate.trustAs("resourceUrl", backend + "/user");
		console.log($scope.submitUrl);
		$scope.$watch(function() {
			return $scope.referredBy;
		}, function(newVal, oldVal) {
			console.log("difference in referredBy", newVal, oldVal);
			api.get('/refer/check/' + newVal).success(function() {
				$scope.user.referredBy = newVal;
			});
		});
		$scope.readyToSubmit = function() {
			return $scope.termsAccepted && ($scope.user.password === $scope.confirmpassword);
		};
		$scope.subscribe = function(planId) {
			$scope.user.planId = planId;
		};
		$scope.registerUser = function() {
			api.post('/user', {
				user: $scope.user,
				payment_method_nonce: $scope.nonce
			}).then(function(resp) {
				console.log("success", resp.data);
				$state.go('authorized', resp.data);
			}, function(err) {
				console.log(err);
				$scope.createError = err.data;
			});

		};
		api.get('/payment/plans').success(function(data) {
			$scope.subscriptions = data.plans;
			console.log(data.plans);
		});
		api.get(clientTokenPath).success(function(data) {
			console.log(data);
			$braintree.setup(data.token, "dropin", {
				container: "payment-form",
				onPaymentMethodReceived: function(resp) {
					//Do some logic in here.
					// When you're ready to submit the form:
					$scope.nonce = resp.nonce;
					$scope.registerUser();
				}
			});


		});

	}
})();
